import React from 'react';
import './App.css';
import TodoForm from './component/TodoForm';


function App() {
  return (
    <div className="App">
      <TodoForm />
      <h1>To Do App</h1>
    </div>
  );
}

export default App;
